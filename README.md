# Truffe2Whiskey

Truffe2Whiskey is a simple synchronization script to replicate accreditations from [Truffe2](https://truffe2.agepoly.ch) to [Whiskey](https://whiskey.agepoly.ch).

## High-level description

Truffe2 is able to export its accreditations into a simple JSON file (e.g. /notes/truffe-accreds-example.json). This scripts uses simple mapping files in /config to translate this representation to the format used by Whiskey (users, units, groups and roles).

### Step-by-step

- Truffe containers execute a cron job that export the accreds. Once exported, the file is posted to the Fission script with a webhook. Command used inside Truffe's container : `python3 manage.py export_accreds > /tmp/accreds.json` and (*WIP : curl to post the JSON*).
- *WIP*