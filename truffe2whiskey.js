require('dotenv').config()
const fs = require('fs')
const { parse } = require('csv-parse/sync')
const axios = require('axios')

const whiskeyApi = process.env.WHISKEY_API || "https://whiskey-api.agepoly.ch"
const configFolder = process.env.CONFIG_FOLDER || __dirname + '/config'
const truffeAccredsFilePath = process.env.TRUFFE_ACCREDS_FILE || __dirname + "/accreds.json"
const truffeAccredsFile = fs.readFileSync(truffeAccredsFilePath)
const truffeAccreds = JSON.parse(truffeAccredsFile).data

const mapGroupsData = fs.readFileSync(configFolder + '/map_groups.txt')
const mapGroups = parse(mapGroupsData, { columns: true, escape: '\\' })
    .filter(g => g.whiskey_group_abv !== 'null')

// console.log("Map groups : ", mapGroups)

const mapUnitsData = fs.readFileSync(configFolder + '/map_units.txt')
const mapUnits = parse(mapUnitsData, { columns: true })
    .filter(u => u.whiskey_unit_code !== 'null')

// console.log("Map groups : ", mapUnits)

const groupsByAbvUnit = {}
var client = axios.create({
    baseURL: `${whiskeyApi}/api`,
    headers: {
        'Content-Type': 'application/json'
    }
})

function addTruffeAccred(truffeAccred, groupAbv, unitCode) {
    const groupKey = `${groupAbv}@${unitCode}`
    const roleName = truffeAccred.role
    if (!groupsByAbvUnit[groupKey]) {
        groupsByAbvUnit[groupKey] = {
            groupAbv,
            roleName,
            unitCode,
            members: [truffeAccred]
        }
    } else {
        groupsByAbvUnit[groupKey].members.push(truffeAccred)
    }
}

const skippedTruffeUnits = []

// Building groupsByAbvUnit
for (const truffeUnit of truffeAccreds) {
    const mappedUnit = mapUnits.find(u => u.truffe_unit_name === truffeUnit.name)
    if (!mappedUnit) {
        // console.log(`Skipping Truffe unit ${truffeUnit.name}`)
        skippedTruffeUnits.push(truffeUnit.name)
        continue
    }
    console.log(`Processing Truffe unit ${truffeUnit.name}`)
    for (const truffeAccred of truffeUnit.accreds) {
        const mappedGroup = mapGroups.find(g => g.truffe_role_name === truffeAccred.role)
        if (!mappedGroup) {
            continue
        }
        if (truffeAccred.end !== 'None') {
            continue
        }
        if (truffeAccred.user.length !== 6 || isNaN(parseInt(truffeAccred.user))) {
            // console.log("Rejecting non-sciper username", truffeAccred.user)
            continue
        }
        addTruffeAccred(truffeAccred, mappedGroup.whiskey_group_abv, mappedUnit.whiskey_unit_code)
    }
}

console.log("Skipped Truffe units : ", skippedTruffeUnits.join(','))

async function whiskeyCreateGroupAndRole(abv, unit, name, createRole) {
    console.log(`Creating group ${abv}@${unit} (${name}, createRole=${createRole})`)

    const groupRes = await client({
        method: 'post',
        url: '/groups',
        data: {
            abv,
            name,
            validator: null,
            unit,
            function: null,
            info: "Created by Truffe2Whiskey script"
        }
    })

    if (!createRole) return
    const groupUid = groupRes.data.uid
    await client({
        method: 'post',
        url: '/roles',
        data: {
            display: name,
            group: groupUid
        }
    })
}

async function whiskeyCreateRoleMembership(unitCode, role, user) {
    console.log(`New membership for unit ${unitCode} :  ${user.display_name} -> ${role.display} (${user.uid}, ${role.uid})`)

    await client({
        method: 'post',
        url: `/roles/${role.uid}/members/${user.uid}`,
        data: {
            main_role: true,
            visible_in_directory: true,
            valid_from: '1970-01-01',
            valid_to: '1970-01-01'
        }
    })
}

async function whiskeyAutoRegisterUsers(scipers) {
    console.log(`Auto-registration of scipers ${scipers.join(',')}`)
    const res = await client({
        method: 'post',
        url: `/batch/ensure-users?scipers=${scipers.join(',')}`,
        data: {}
    })

    const newScipers = res.data.users.map(u => u.sciper)
    if (newScipers.length > 0) {
        console.log(`New users created for ${groupKey}: ${newScipers.join(",")}`)
    }

    const unknownScipers = res.data.unknown_scipers
    if (unknownScipers.length > 0) {
        console.log(`Unknown scipers found for ${groupKey}: ${unknownScipers.join(",")}`)
    }
}

axios({
    method: 'post',
    url: `${whiskeyApi}/api/login`,
    data: {
        username: process.env.WHISKEY_API_USER,
        password: process.env.WHISKEY_API_PASS,
    }
}).then(async (res) => {
    // console.log("JWT: " + res.data.jwt)
    client = axios.create({
        baseURL: `${whiskeyApi}/api`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + res.data.jwt
        }
    })

    var groupsRes = await client.get('/groups')
    var groups = groupsRes.data
    var rolesRes = await client.get('/roles')
    var roles = rolesRes.data
    var allScipers = []
    for (const groupKey in groupsByAbvUnit) {
        const g = groupsByAbvUnit[groupKey]
        if (!groups.find(g => `${g.abv}@${g.unit_code}` === groupKey)) {
            const createRole = !roles.find(r => r.display === g.roleName)
            await whiskeyCreateGroupAndRole(g.groupAbv, g.unitCode, g.roleName, createRole)
        }
        for (const truffeAccred of g.members) {
            const sciper = truffeAccred.user
            if (!allScipers.includes(sciper)) {
                allScipers.push(sciper)
            }
        }
    }
    await whiskeyAutoRegisterUsers(allScipers)

    groupsRes = await client.get('/groups')
    groups = groupsRes.data
    rolesRes = await client.get('/roles')
    roles = rolesRes.data
    const membershipsRes = await client.get('/roles/memberships')
    const memberships = membershipsRes.data
    const usersRes = await client.get('/users')
    const users = usersRes.data

    for (const groupKey in groupsByAbvUnit) {
        const group = groupsByAbvUnit[groupKey]
        const role = roles.find(r => r.display === group.roleName)
        for (const truffeAccred of group.members) {
            const user = users.find(u => u.sciper?.toString() === truffeAccred.user)
            if (!user) {
                console.error("User not found", truffeAccred)
                console.log(users)
                return
            }
            await whiskeyCreateRoleMembership(group.unitCode, role, user)
        }
    }

}).catch(err => {
    throw err
})